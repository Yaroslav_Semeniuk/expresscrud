const express = require('express')
const bodyParser = require('body-parser')
const router = require('./api/routes/listRoutes')
const {PORT} = require("./api/constants")

const server = express()
server.listen(PORT)
console.log('Server started on: ', PORT)

// Middleware
server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())

server.use('/', router) // все что будет попадать на "/" будет пересылаться в роуты

server.use((req, res) => {
    res.status(404).send({url: req.originalUrl + ' not found'})
})
