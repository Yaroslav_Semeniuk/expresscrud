const {listAllNotes, createNote, readNote, updateNote, deleteNote} = require("../controllers/controller");
const { Router } = require('express');
const router = Router();

router.route('/notes')
    .get(listAllNotes)
    .post(createNote)

router.route('/notes/:noteId')
    .get(readNote)
    .put(updateNote)
    .delete(deleteNote)

module.exports = router