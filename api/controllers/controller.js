const {getNotes, createN, readN, updateN, deleteN} = require("../models/noteModel");

exports.listAllNotes = async (req, res, next) => {
    const {result, err} = await getNotes()
    if (err) {
        return res.send(err)
    }
    res.json(result)
}

exports.createNote = async (req, res, next) => {
    const {result, err} = await createN(req)
    if (err) {
        return res.send(err)
    }
    res.json(result)
}

exports.readNote = async (req, res, next) => {
    const {result, err} = await readN(req)
    if (err) {
        return res.send(err)
    }
    res.json(result)
}

exports.updateNote = async (req, res, next) => {
    const {result, err} = await updateN(req)
    if (err) {
        return res.send(err)
    }
    res.json(result)
}

exports.deleteNote = async (req, res, next) => {
    const {result, err} = await deleteN(req)
    if (err) {
        return res.send(err)
    }
    res.json(result)
}