const mongoose = require('mongoose');
const {URI} = require("../constants");

mongoose.connect(URI, {useNewUrlParser: true, useUnifiedTopology: true}).then(
    () => {
        console.log('DB connected')
    }, err => {
        console.log('Error connect to DB', err)
    })

module.exports = {models: {Note: require('../database/schemes/noteScheme')}}