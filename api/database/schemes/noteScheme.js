const mongoose = require('mongoose')
const Schema = mongoose.Schema

const noteSchema = new Schema({
    title: {
        type: String,
        required: true,
        index: true
    },
    text: String,
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        index: true,
        ref: 'Author'
    },
    created: {
        type: Date,
        default: Date.now
    }
});

noteSchema.index({author: 1, title: 1}, {unique: true})
module.exports = mongoose.model('Note', noteSchema)

