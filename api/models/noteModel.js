const {Note} = require('../database/index').models

exports.getNotes = async () => {
    try {
        return {result: await Note.find({}).lean()}  // позволяет вернуть JS формат документа, а не монго-формат
    } catch (err) {
        return {err}
    }
}

exports.createN = async (req) => {
    try {
        const addNote = new Note(req.body)
        return {result: await addNote.save()}
    } catch (err) {
        return {err}
    }
}

exports.readN = async (req) => {
    try {
        return {result: await Note.findOne({"_id":req.params.noteId}).lean()}
    } catch (err) {
        return {err}
    }
}

exports.updateN = async (req) => {
    try {
        return {result: await Note.findOneAndUpdate({"_id":req.params.noteId},  req.body,{new: true}).lean()}
    } catch (err) {
        return {err}
    }
}

exports.deleteN = async (req) => {
    try {
        return {result: await Note.findOneAndDelete({"_id":req.params.noteId}).lean()}
    } catch (err) {
        return {err}
    }
}